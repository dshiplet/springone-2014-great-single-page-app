package com.example.rest.api.exceptions;

import com.example.rest.api.RestApiException;
import com.example.rest.api.RestApiHttpStatus;

public class ResourceNotFoundRestApiException extends RestApiException
{
	private static final long serialVersionUID = 1L;

	public ResourceNotFoundRestApiException()
	{
		super(RestApiHttpStatus.NOT_FOUND);
	}
}
