package com.example;

import static com.example.spa.site.SiteUrls.LOGIN;
import static com.example.spa.site.SiteUrls.LOGIN_ERROR;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.security.ApiAwareAuthenticationEntryPoint;
import com.example.security.ApiAwareAuthenticationFailureHandler;
import com.example.security.ApiAwareAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableWebMvcSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("user").password("password").roles("USER")
			.and()
			.withUser("admin").password("password").roles("ADMIN"); 
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
		    .antMatchers(LOGIN,LOGIN_ERROR,"/static/**","webjars/**","/logout","/lib/**").permitAll()
		    .anyRequest().authenticated()
		    .and()
		    .formLogin()
		    .defaultSuccessUrl("/")
			.successHandler(new ApiAwareAuthenticationSuccessHandler())
			.failureHandler(new ApiAwareAuthenticationFailureHandler())
			.and()	
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.and()
			.exceptionHandling().authenticationEntryPoint(new ApiAwareAuthenticationEntryPoint())
			.and()
			.csrf().disable();
	}

}