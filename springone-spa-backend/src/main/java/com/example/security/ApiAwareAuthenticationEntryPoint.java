package com.example.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import com.example.rest.api.ApiErrorCodes;
import com.example.rest.api.RestApiError;
import com.example.rest.api.RestApiHttpStatus;
import com.example.util.JacksonUtils;

public class ApiAwareAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint
{

	public ApiAwareAuthenticationEntryPoint()
	{
		super("/login");
	}

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException
	{
		String requestURI = request.getRequestURI();
		String contextPath = request.getContextPath();

		String apiPath = contextPath + "/api/";
		boolean isApi = requestURI.startsWith(apiPath);

		String uiPath = contextPath + "/ui";
		boolean isUi = requestURI.startsWith(uiPath);
		if (isUi || isApi)
		{
			RestApiError restApiError = new RestApiError(RestApiHttpStatus.UNAUTHORIZED);
			restApiError.setApiCode(ApiErrorCodes.SESSION_EXPIRED);
			restApiError.setDeveloperMessage("The server side http session has expired, user needs to login in again");
			restApiError.setUserMessage("<strong>Your account was signed out for your security, because of a lack of activity.</strong>");

			String json = JacksonUtils.toJSON(restApiError);
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.getWriter().println(json);

		} else
		{
			super.commence(request, response, authException);
		}
	}
}
