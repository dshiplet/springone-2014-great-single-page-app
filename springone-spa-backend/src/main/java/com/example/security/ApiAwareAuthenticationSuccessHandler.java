package com.example.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class ApiAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler
{
	public static final String X_SPA_API_LOGIN = "X-spa-api-login";

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException
	{

		String apiLoginHeader = request.getHeader(X_SPA_API_LOGIN);
		if ("true".equals(apiLoginHeader))
		{
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			response.setStatus(HttpStatus.OK.value());
			response.getWriter().println("{\"success\":\"true\"}");
		} else
		{
			super.onAuthenticationSuccess(request, response, authentication);
		}
	}
}
